from types import resolve_bases
from flask import Flask, Response, render_template
import mimetypes
import dataAutogen
import json
import os
app = Flask(__name__)

@app.route('/')
def index():
    #default lang: korean(ko)
    try:
        langJson = json.load(open(os.getcwd() + '/langpack/index.html.lang.json','r',encoding='utf-8'))['ko']
        content = json.load(open(os.getcwd() + '/web_contents/index.html.web.json','r',encoding='utf-8'))['ko']
        content = dataAutogen.proc(content)
        header = render_template('header.jinja2',lang=langJson)
    except (KeyError, FileNotFoundError, json.JSONDecodeError):
        return Response('not found',status=404)
    return Response(render_template('index.jinja2',lang=langJson,header=header,content=content))
@app.route('/<lang>')
def indexWithLang(lang=None):
    #if only lang is assigned in URI
    try:
        langJson = json.load(open(os.getcwd() + '/langpack/index.html.lang.json','r',encoding='utf-8'))[lang]
        header = render_template('header.jinja2',lang=langJson)
        content = json.load(open(os.getcwd() + '/web_contents/index.html.web.json','r',encoding='utf-8'))[lang]
        content = dataAutogen.proc(content)
        return Response(render_template('index.jinja2',lang=langJson,header=header,content=content))
    except (KeyError, FileNotFoundError, json.JSONDecodeError):
        return Response('not found',status=404)
@app.route('/<lang>/<path>')
def indexWithPathAndLang(lang=None,path=None):
    #if both path and lang is assigned in URI
    try:
        mime = mimetypes.guess_type(path)[0]
        if mime == 'text/html':
            #HTML doc
            try:
                langJson = json.load(open(os.getcwd() + '/langpack/' + path + '.lang.json','r',encoding='utf-8'))[lang]
                header = render_template('header.jinja2',lang=langJson)
                content = json.load(open(os.getcwd() + '/web_contents/' + path + '.web.json','r',encoding='utf-8'))[lang]
                #generate data changes automatically
                content = dataAutogen.proc(content)
                return Response(render_template(path.replace('html','jinja2'),lang=langJson,header=header,content=content))
            except (KeyError, FileNotFoundError, json.JSONDecodeError):
                return Response(render_template(path.replace('html','jinja2')))
        #mimetype guess available
        elif mime != None:
            return Response(open(os.getcwd() +'/statics/'+ path,'rb').read(),mimetype=mime)
        #mimetype guess unavailable
        else:
            return Response(open(os.getcwd() +'/statics/'+ path,'rb').read())
    except FileNotFoundError:
        return Response('not found',status=404)
#API start
@app.route('/api')
def apiIndex():
    return Response('API')
@app.route('/api/lang/<path>')
def apiLang(path=None):
    try:
        return Response(open(os.getcwd()+'/langpack/'+path.replace('.json','',1)+'.lang.json',encoding='utf-8'),mimetype='text/json')
    except FileNotFoundError:
        return Response('not found',status=404)