lang = {'ko':{'faceTheFear':'공포에 직면하여','buildTheFuture':'미래를 창조하라','langSwitch':'English','langSwitchPath':'en','selfLang':'ko'},
        'en':{'faceTheFear':'Face the fear','buildTheFuture':'Build the future','langSwitch':'Korean','langSwitchPath':'ko','selfLang':'en'}}
import json
import os
json.dump(lang,open(os.getcwd()+'/langpack/index.html.lang.json','w'))